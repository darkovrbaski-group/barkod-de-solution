package flights.topology;

import flights.serde.Serde;
import java.sql.Date;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Properties;

import java.util.TimeZone;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import radar.AirportKpi;
import radar.AirportUpdateEvent;
import radar.FlightKpi;
import radar.FlightUpdateEvent;

public class TopologyBuilder implements Serde {

  private Properties config;

  public TopologyBuilder(Properties properties) {
    this.config = properties;
  }

  private static final Logger logger = LogManager.getLogger(TopologyBuilder.class);

  public Topology build() {
    StreamsBuilder builder = new StreamsBuilder();
    String schemaRegistry = config.getProperty("kafka.schema.registry.url");

    KStream<String, FlightUpdateEvent> flightInputStream = builder.stream(
        config.getProperty("kafka.topic.flight.update.events"),
        Consumed.with(Serde.stringSerde,
            Serde.specificSerde(FlightUpdateEvent.class, schemaRegistry)));

    GlobalKTable<String, AirportUpdateEvent> airportTable = builder.globalTable(
        config.getProperty("kafka.topic.airport.update.events"),
        Consumed.with(Serde.stringSerde,
            Serde.specificSerde(AirportUpdateEvent.class, schemaRegistry)));

    flightInputStream
        .filter((k, v) -> !v.getStatus().toString().equals("CANCELED"))
        .mapValues((k, v) -> transform(v))
        .to(config.getProperty("kafka.topic.radar.flights"));

    flightInputStream
        .filter((k, v) -> !v.getStatus().toString().equals("LATE") || !v.getStatus().toString().equals("CANCELED"))
        .mapValues((k, v) -> transform(v))
        .selectKey((k, v) -> v.getDepartureAirportCode())
        .groupByKey()
        .windowedBy(TimeWindows.of(Duration.of(5, ChronoUnit.MINUTES)).advanceBy(Duration.of(1, ChronoUnit.MINUTES)))
        .count()
        .mapValues((k, v) -> v)
        .toStream().foreach((k, v) -> logger.info("Departure airport: " + k.key() + " " + k.window().start() + " " + k.window().end() + " " + v));

    return builder.build();
  }

  private static FlightKpi transform(FlightUpdateEvent fe) {
    FlightKpi fk = new FlightKpi();
    fk.setTo(fe.getDestination().toString().split("->")[0]);
    fk.setFrom(fe.getDestination().toString().split("->")[1]);
    fk.setDepartureTimestamp(fe.getSTD());
    fk.setArrivalTimestamp(fe.getSTA());
    LocalDateTime duration = LocalDateTime.ofInstant(Instant.ofEpochSecond(fe.getSTA() - fe.getSTD()), ZoneId.systemDefault());
    fk.setDuration(duration.getMinute());
    fk.setDepartureAirportCode(fe.getDestination().toString().split("\\)->")[0].split("\\(")[1]);
    fk.setArrivalAirportCode(fe.getDestination().toString().split("->")[1].split("\\(")[1].replace(")", ""));

    fk.setDepartureDatetime(LocalDateTime.ofInstant(Instant.ofEpochMilli(fe.getSTD()), ZoneId.of(fe.getTimezones().toString().split("->")[0])).toString());
    fk.setArrivalDatetime(LocalDateTime.ofInstant(Instant.ofEpochMilli(fe.getSTA()), ZoneId.of(fe.getTimezones().toString().split("->")[1])).toString());

    fk.setAirline(fe.getAirline());
    fk.setGate(fe.getGate());
    fk.setStatus(fe.getStatus());
    fk.setId(fe.getId());
    fk.setTimezones(fe.getTimezones());
    return fk;
  }

  private static AirportKpi transform2(FlightUpdateEvent fe) {
    AirportKpi ak = new AirportKpi();
    ak.setDeparturesLast5Minutes(1L);

    return ak;
  }
}
